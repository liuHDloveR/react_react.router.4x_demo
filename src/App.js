import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, HashRouter, Switch } from 'react-router-dom';


import ComponentHeader from './components/header';
import ComponentFooter from './components/footer';
import BodyIndex from './components/bodyindex';
import ComponentList from './components/list';
import ComponentDetails from './components/details';

import 'antd/dist/antd.css';
import './css/style.css';

class Home extends Component {
  render() {
    return (
      <div>
        <ComponentHeader />
        <BodyIndex userid={123456} username={'Nike'} />
        <div>
          {this.props.children}
        </div>
        <ComponentFooter />
      </div>
    )
  }
}

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Switch>
            <Route exact path='/' component={Home}></Route>
            <Route path={'/list/:id'} component={ComponentList}></Route>      
            <Home>
              <Route path='/details' component={ComponentDetails}></Route>
            </Home>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
