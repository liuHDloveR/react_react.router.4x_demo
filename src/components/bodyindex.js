import React from 'react';
import PropTypes from 'prop-types';
import ReactMixin from 'react-mixin';

import { Input } from 'antd';

import BodyChild from './bodychild';
import MixinLog from './mixins';
class BodyIndex extends React.Component {

  // 写法一
  // static defaultProps = {
  //   username: '这是一个默认的用户名'
  // };

  constructor() {
    super();  // 调用基类的所有的初始化方法
    this.state = {
      username: 'Parry',
      age: 10
    }; // 初始化赋值
  };

  changeUserInfo(age) {
    this.setState({ age: age });
    // 第一种方式
    // var mySubmitBotton = document.getElementById('submitButton');
    // console.log(mySubmitBotton);
    // ReactDOM.findDOMNode(mySubmitBotton).style.color = 'red';

    // 第二种方式
    console.log(this.refs.submitButton);
    this.refs.submitButton.style.color = 'red';

    MixinLog.log();
  };

  handleChildValueChange(event) {
    this.setState({ age: event.target.value });
  };

  render() {
    // setTimeout(() => {
    //   this.setState({ username: 'IMOOC', age: 20 });
    // }, 4000)

    return (
      <div>
        <h2>这里是内容</h2>
        <p>接收到的父页面的属性：userid:{this.props.userid} username:{this.props.username}</p>
        <p>age:{this.state.age}</p>
        <Input placeholder="Basic usage" />
        <input id="submitButton" ref="submitButton" type='button' value='提交' onClick={this.changeUserInfo.bind(this, 99)} />
        <BodyChild {...this.props} id={4} handleChildValueChange={this.handleChildValueChange.bind(this)} />
      </div>
    )
  }
}

BodyIndex.propTypes = {
  userid: PropTypes.number.isRequired
};
// 写法二
const defaultProps = {
  username: '这是一个默认的用户名'
}
BodyIndex.defaultProps = defaultProps;

ReactMixin(BodyIndex.prototype, MixinLog);

export default BodyIndex;