import React from 'react';

class ComponentList extends React.Component {
  render() {
    return (
      <div>
        <h2>这里是列表页面，ID：{this.props.match.params.id}</h2>
      </div>
    );
  }
}

export default ComponentList;