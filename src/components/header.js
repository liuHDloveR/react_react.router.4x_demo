import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

class ComponentHeader extends React.Component {
  constructor() {
    super();
    this.state = {
      minHeader: false    // 默认加载的时候不是mini的头部
    }
  };

  switchHeader() {
    this.setState({
      minHeader: !this.state.minHeader
    })
  };

  render() {
    const styleComponentHeader = {
      header: {
        backgroundColor: '#333',
        color: '#fff',
        paddingTop: (this.state.minHeader) ? '3px' : '15px',
        paddingBottom: (this.state.minHeader) ? '3px' : '15px'
      }
      // 还可以定义其它的样式
    };
    return (
      <header>
        <h1 style={styleComponentHeader.header} className='smallFontSize' onClick={this.switchHeader.bind(this)}>这里是头部</h1>
        <p><Link to='/'>首页</Link></p>
        <p><Link to='/list/123'>列表</Link></p>
        <p><Link to='/details'>详情</Link></p>
      </header>
    )
  }
}

export default ComponentHeader;