import React from 'react';

import '../css/footer.css';
class ComponentFooter extends React.Component {
  render() {

    var footerConverStyle = {
      "miniFooter": {
        "background": "red",
        "color": "#fff",
        "padding": "3px 0 3px 20px"
      },
      "miniFooter_h1": {
        "fontSize": "15px"
      }
    }

    // console.log(footerCss);
    return (
      <footer className='miniFooter' style={footerConverStyle.miniFooter}>
        <h1>这里是页脚</h1>
      </footer>
    )
  }
}

export default ComponentFooter;